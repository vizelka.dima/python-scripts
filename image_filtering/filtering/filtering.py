import numpy as np


def apply_one_channel(img_channel: np.array, kernel: np.array, output_image: np.array):
    kernel_size = kernel.shape[0]
    pad_lu = int(kernel_size // 2)
    pad_rd = int(pad_lu + ((kernel_size + 1) % 2))

    img_channel_padded = np.pad(img_channel, ((pad_lu, pad_rd),), 'constant', constant_values=0)

    rows = img_channel.shape[0]
    cols = img_channel.shape[1]

    for row in range(rows):
        for col in range(cols):
            img_cut = img_channel_padded[row:(row + kernel_size), col:(col + kernel_size)]
            val = np.sum(img_cut * kernel)
            if val > 255:
                val = 255
            elif val < 0:
                val = 0
            output_image[row, col] = val


def apply_filter(image: np.array, kernel: np.array) -> np.array:
    # A given image has to have either 2 (grayscale) or 3 (RGB) dimensions
    assert image.ndim in [2, 3]
    # A given filter has to be 2 dimensional and square
    assert kernel.ndim == 2
    assert kernel.shape[0] == kernel.shape[1]

    output_image = np.zeros_like(image)

    if image.ndim == 2:
        apply_one_channel(image, kernel, output_image)
    else:
        apply_one_channel(image[..., 0], kernel, output_image[..., 0])
        apply_one_channel(image[..., 1], kernel, output_image[..., 1])
        apply_one_channel(image[..., 2], kernel, output_image[..., 2])

    return output_image
